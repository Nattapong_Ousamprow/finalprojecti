﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using SpaceShip;


namespace Enemy
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private EnemySpaceShip enemySpaceShip;
        [SerializeField] private float chasingTresholdDistance;
        //[SerializeField] private PlayerSpaceShip playerSpaceShip;
        private PlayerSpaceShip spawnedPlayerShip;

        public void Init(PlayerSpaceShip playerSpaceShip)
        {
            spawnedPlayerShip = playerSpaceShip;
        }

        private void Update()
        {
            MoveToPlayer();
            enemySpaceShip.Fire();
            
        }

        private void MoveToPlayer()
        {
           //var distanceToPlayer = Vector2.Distance(spawnedPlayerShip.transform.position, transform.position);
            //Debug.Log(distanceToPlayer);

          //  if (distanceToPlayer < chasingTresholdDistance)
          // {
           //     var direction = (Vector2)(spawnedPlayerShip.transform.position - transform.position);
          //      direction.Normalize();
          //      var distance = direction * enemySpaceShip.Speed * Time.deltaTime;
          //      gameObject.transform.Translate(distance);
          //  }
        }
        /* [SerializeField] private Transform playerTransform;
         [SerializeField] private float enemyShipSpeed = 5;
         [SerializeField] private Renderer playerRenderer;


         private Renderer enemyrenderer;
         private float chasingThresholdDistance = 1.0f;

         private void Awake()
         {
             enemyrenderer = GetComponent<Renderer>();
         }
         void Update()
         {
             MoveToPlayer();
         }

         private void OnDrawGizmos()
         {
             CollisonDebug();
         }
         private void CollisonDebug()
         {
             if (enemyrenderer != null && playerRenderer != null)
             {
                 if (intersectAABB(enemyrenderer.bounds, playerRenderer.bounds))
                 {
                     Gizmos.color = Color.red;
                 }
                 else
                 {
                     Gizmos.color = Color.white;
                 }

                 Gizmos.DrawWireCube(enemyrenderer.bounds.center, 2 * enemyrenderer.bounds.extents);
                 Gizmos.DrawWireCube(playerRenderer.bounds.center, 2 * playerRenderer.bounds.extents);
             }
         }

         private bool intersectAABB(Bounds a, Bounds b)
         {
             return (a.min.x <= b.max.x && a.max.x >= b.min.x) && (a.min.y <= b.max.y && a.max.y >= b.min.y);
         }

         private void MoveToPlayer()
         {
             Vector3 enemyPosition = transform.position;
             Vector3 playerPosition = playerTransform.position;
             enemyPosition.z = playerPosition.z; // ensure there is no 3D rotation by aligning Z position

             Vector3 vectorToTarget = playerPosition - enemyPosition; // vector from this object towards the target location
             Vector3 directionToTarget = vectorToTarget.normalized;
             Vector3 velocity = directionToTarget * enemyShipSpeed;

             float distanceToTarget = vectorToTarget.magnitude;

             if (distanceToTarget > chasingThresholdDistance)
             {
                 transform.Translate(velocity * Time.deltaTime);
             }

         }*/
    }
}
