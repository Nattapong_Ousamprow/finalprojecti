﻿using UnityEngine.Analytics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using SpaceShip;
using Enemy;

namespace Manager
{

    public class GameManager : MonoBehaviour
    {
        [SerializeField] private Button startButton;
        [SerializeField] private RectTransform dialog;
        [SerializeField] private PlayerSpaceShip playerSpaceShip;
        [SerializeField] private EnemySpaceShip enemySpaceShip;
        [SerializeField] private ScoreManager scoreManager;
        public event Action OnRestarted;
        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemySpaceshipMoveSpeed;

        private PlayerSpaceShip spawnedPlayerShip;
        private void Awake()
        {
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(dialog != null, "dialog cannot be null");
            Debug.Assert(playerSpaceShip != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceShip != null, "enemySpaceship cannot be null");
            Debug.Assert(scoreManager != null, "scoreManager cannot be null");
            Debug.Assert(playerSpaceshipHp > 0, "playerSpaceShipHp has to be more than zero");
            Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed has to be more than zero");
            Debug.Assert(enemySpaceshipHp > 0, "enemySpaceshipHp has to be more than zero");
            Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed has to be more than zero");

            startButton.onClick.AddListener(OnStartButtonClicked);

            SoundManager.Instance.PlayBGM();
        }

        private void OnStartButtonClicked()
        {
            dialog.gameObject.SetActive(false);
            StartGame();
        }
        private void StartGame()
        {
            scoreManager.Init(this);
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
        }

        private void SpawnPlayerSpaceship()
        {
            spawnedPlayerShip = Instantiate(playerSpaceShip);
            spawnedPlayerShip.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spawnedPlayerShip.OnExploded += OnPlayerSpaceshipExploded;
        }
        private void OnPlayerSpaceshipExploded()
        {
            Restart();
        }

        private void SpawnEnemySpaceship()
        {
            var spawnedEnemyShip = Instantiate(enemySpaceShip);
            spawnedEnemyShip.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
            spawnedEnemyShip.OnExploded += OnEnemySpaceshipExploded;

            var enemyController = spawnedEnemyShip.GetComponent<EnemyController>();
            enemyController.Init(spawnedPlayerShip);
        }
        private void OnEnemySpaceshipExploded()
        {
            scoreManager.SetScore(1);
        }
        private void Restart()
        {
            DestroyRemainingShips();
            dialog.gameObject.SetActive(true);
            OnRestarted?.Invoke();
        }
        private void DestroyRemainingShips()
        {
            var remainingEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach(var enemy in remainingEnemies)
            {
                Destroy(enemy);
            }
            var remainingPlayers = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in remainingPlayers)
            {
                Destroy(player);
            }
        }
        /*private void GameOver(string winningPlayer)
        {
            
            
            SetGameOverText(winningPlayer + "Wins!");
            
            restartButton.SetActive(true);

        }
        void SetGameOverText(string value)
        {
            gameOverPanel.SetActive(true);
            gameOverText.text = value;
        }*/
    }
}

