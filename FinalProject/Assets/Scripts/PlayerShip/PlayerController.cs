﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using SpaceShip;

namespace Player
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private PlayerSpaceShip playerSpaceShip;
        private Vector2 movementInput = Vector2.zero;
        private ShipInputActions inputActions;
        private float xMin;
        private float xMax;
        private float yMin;
        private float yMax;

        private void Awake()
        {
            InitInput();
            CreateMovementBoundary();
        }

        private void InitInput()
        {
            inputActions = new ShipInputActions();
            inputActions.Player.Move.performed += OnMove;
            inputActions.Player.Move.canceled += OnMove;
            inputActions.Player.Fire.performed += OnFire;
        }

        private void OnFire(InputAction.CallbackContext obj)
        {
            playerSpaceShip.Fire();
        }


        private void Update()
        {
            Move();
        }

        private void CreateMovementBoundary()
        {
            var mainCamera = Camera.main;
            Debug.Assert(mainCamera != null, "Main camera cannot be null");

            var spriteRenderer = playerSpaceShip.GetComponent<SpriteRenderer>();
            Debug.Assert(spriteRenderer != null, "spriteRenderer cannot be null");

            var offset = spriteRenderer.bounds.size;
            xMin = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).x + offset.x / 2;
            xMax = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).x - offset.x / 2;
            yMin = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).y + offset.y / 2;
            yMax = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).y - offset.y / 2;

        }

        private void Move()
        {

            var inPutVelocity = movementInput * playerSpaceShip.Speed;
            var newPosition = transform.position;
            newPosition.x = transform.position.x + inPutVelocity.x * Time.smoothDeltaTime;
            newPosition.y = transform.position.y + inPutVelocity.y * Time.smoothDeltaTime;



            newPosition.x = Mathf.Clamp(newPosition.x, xMin, xMax);
            newPosition.y = Mathf.Clamp(newPosition.y, yMin, yMax);

            transform.position = newPosition;

        }

        public void OnMove(InputAction.CallbackContext obj)
        {
            if (obj.performed)
            {
                movementInput = obj.ReadValue<Vector2>();
            }

            if (obj.canceled)
            {
                movementInput = Vector2.zero;
            }
        }

        public void OnEnable()
        {
            inputActions.Enable();
        }

        public void OnDisable()
        {
            inputActions.Disable();
        }

    }
}