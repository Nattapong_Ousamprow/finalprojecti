﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
namespace SpaceShip
{
    public interface IDamagable
    {
        event Action OnExploded;
        void TakeHit(int damage);
        void Explode();
    }
}