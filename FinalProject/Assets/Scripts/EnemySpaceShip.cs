﻿using System.Collections.Generic;
using UnityEngine;
using System;

namespace SpaceShip
{

    public class EnemySpaceShip : BaseSpaceShip, IDamagable
    {

        public event Action OnExploded;
        [SerializeField] private double fireRate = 1;
        private float fireCounter = 0;
        [SerializeField] private AudioClip enemyDieSound;
        [SerializeField] private float enemyDieSoundVol = 0.2f;
        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }
        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }
        public void Explode()
        {
            AudioSource.PlayClipAtPoint(enemyDieSound, Camera.main.transform.position, enemyDieSoundVol);
            Debug.Assert(Hp <= 0, "Hp is more than zero");
            gameObject.SetActive(false);
            Destroy(gameObject);
            OnExploded?.Invoke();
        }
        public override void Fire()
        {
            fireCounter += Time.deltaTime;
            if (fireCounter >= fireRate)
            {
                var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
                bullet.Init(Vector2.down);
                fireCounter = 0;
            }
        }
    }
}

