﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Manager;

namespace SpaceShip
{
    public class PlayerSpaceShip : BaseSpaceShip, IDamagable
    {
        public event Action OnExploded;
        [SerializeField] private AudioClip playerDieSound;
        [SerializeField] private float playerDieSoundVol = 0.2f;
       
        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
            audioSource = gameObject.GetComponent<AudioSource>();
        }
        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public override void Fire()
        {
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Init(Vector2.up);
            SoundManager.Instance.Play(audioSource, SoundManager.Sound.PlayerFire);
            
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }
        public void Explode()
        {
            AudioSource.PlayClipAtPoint(playerDieSound, Camera.main.transform.position,playerDieSoundVol);
            Debug.Assert(Hp <= 0, "Hp is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke();
        }
    }
}
